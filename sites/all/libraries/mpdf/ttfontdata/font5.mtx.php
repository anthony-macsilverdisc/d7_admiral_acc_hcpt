<?php
$name='CrimsonText-Roman';
$type='TTF';
$desc=array (
  'Ascent' => 1002,
  'Descent' => -290,
  'CapHeight' => 599,
  'Flags' => 4,
  'FontBBox' => '[-273 -290 1197 1002]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 365,
);
$up=-132;
$ut=46;
$ttffile='/usr/local/apache/virtualhosts/drupal7_generic/drupal7_accdev/sites/all/libraries/mpdf/ttfonts/Crimson_Text.ttf';
$TTCfontID='0';
$originalsize=189604;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='font5';
$panose=' 1 2 2 0 5 3 0 0 0 0 0 0';
$haskerninfo=false;
$unAGlyphs=false;
?>