name = Ecommerce
description = A simple Ecommerce layout.
preview = preview.png
template = ecommerce-layout

; Regions
regions[cart]           = Cart
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[slideshow]      = Slideshow
regions[breadcrumb]     = Breadcrumb
regions[content_first]  = Content first
regions[content]        = Content
regions[blogrelated]    = Blog Related
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer_1]       = Footer 1
regions[footer_2]       = Footer 2
regions[footer_3]       = Footer 3
regions[footer_4]       = Footer 4
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/ecommerce/ecommerce.layout.css
