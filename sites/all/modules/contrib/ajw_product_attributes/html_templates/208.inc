<body>
<div id="left-panel" style="padding-top:130px;max-height: 1737px; height: 1737px; width: 50%; float: left;">
    <h4>Custom vertical layout</h4>
    <div id="left-panel-image">
        <h4>Image(s)</h4>
            <? if (isset($horizontal_left_panel_image) && $horizontal_left_panel_image != '') { ?>
                <div style="width:1472px; height:826px; margin-left:auto;margin-right:auto;text-align:center"><?php echo $horizontal_left_panel_image ?></div>
            <? } ?>
    
             <? if ($horizontal_signature_image != '') { ?>
                <div id="left-image" style="height:293px;margin-bottom:31px; width:586px;padding-top:150px">
                    <?php echo $horizontal_signature_image ?></div>
            <? } ?>
            <? if ($centre_bottom != '') { ?>
                <div id="left-image" style="height:293px;margin-bottom:31px; width:586px;padding-top:150px">
                    <?php echo $centre_bottom ?></div>
            <? } ?>
                
   </div>
</div>
<div id="right-panel" style="padding-top: 130px;max-height:1737px;height:1737px;width:50%;float:left;">
    <h4>Greeting</h4>
    <p>&nbsp;</p>
        <div id="greeting" style="text-align:left;width:2240px;height:574px">
            <?php echo $greeting ?>
        </div>
    <h4>Address</h4>
        <div id="address" style="text-align:left;height:574px;width:2240px;">
            <?php echo $address ?>
        </div>
</div>
</body>