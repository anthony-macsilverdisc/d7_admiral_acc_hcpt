<?php
/**
 * @file
 * admiral_taxonomy.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */


/**
 * Implements hook_views_default_views().
 */
function admiral_taxonomy_views_default_views() {
    // exported view goes here
    $view = new view();
    $view->name = 'card_collections';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'search_api_index_product_display';
    $view->human_name = 'Card Collections';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Card Collections';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '40';
    $handler->display->display_options['style_plugin'] = 'list';
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'product_list';
    /* Field: Indexed Node: Node ID */
    $handler->display->display_options['fields']['nid']['id'] = 'nid';
    $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_product_display';
    $handler->display->display_options['fields']['nid']['field'] = 'nid';
    /* Contextual filter: Indexed Node: Product category */
    $handler->display->display_options['arguments']['field_product_category']['id'] = 'field_product_category';
    $handler->display->display_options['arguments']['field_product_category']['table'] = 'search_api_index_product_display';
    $handler->display->display_options['arguments']['field_product_category']['field'] = 'field_product_category';
    $handler->display->display_options['arguments']['field_product_category']['default_action'] = 'not found';
    $handler->display->display_options['arguments']['field_product_category']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['field_product_category']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['field_product_category']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['field_product_category']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['field_product_category']['validate_options']['vocabularies'] = array(
        'product_category' => 'product_category',
    );
    $handler->display->display_options['arguments']['field_product_category']['break_phrase'] = 0;
    $handler->display->display_options['arguments']['field_product_category']['not'] = 0;

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'card-collections/%';

// return views
    $views[$view->name] = $view;
    return $views;
}