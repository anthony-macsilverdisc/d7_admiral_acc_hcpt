<?php
/**
 * @param $form
 * @param $form_state
 * @param null $args
 * @return mixed
 * Provides form for both updating and modifying content
 * $args in the parameters is for editing a specific page
 */
function admiral_landing_pages_form($form, &$form_state, $args = NULL)
{
    //Charity
    $options = array();
    $vocabulary = taxonomy_vocabulary_machine_name_load('charity');
    $charities = taxonomy_get_tree($vocabulary->vid);
    $options[0] = 'Select Charity --';
    foreach ($charities as $charity) {
        $options[$charity->tid] = $charity->name;
    }

    //Load landing pages if arg in url
    $landing_pages = variable_get('admiral_landing_pages', '');

    if ($landing_pages != '' && $args != null) {
        $form_state['storage']['arg'] = $args;
        $landing_pages = explode(',', $landing_pages);
        $landing_page_name = $landing_pages[$args];
        $landing_page_terms = taxonomy_get_term_by_name($landing_page_name, 'landingpages');
        //Get the loaded term id so we can load it fully
        if (!empty($landing_page_terms)) {
            $landing_page_terms = array_values($landing_page_terms);
            $tid = $landing_page_terms[0]->tid;
        }
        //Load the landing page
        $landing_page_term = taxonomy_term_load($tid);
    }

    //Charity select
    $form['charity'] = array(
        '#type' => 'select',
        '#title' => t('Select Charity'),
        '#options' => $options,
        '#default_value' => isset($landing_page_term->admiral_charity_select[LANGUAGE_NONE][0]['tid']) ? $landing_page_term->admiral_charity_select[LANGUAGE_NONE][0]['tid'] : $options[0],
        '#description' => t('Select the <em>Charity</em> which will have the Landing Page added.'),
    );


    $form['page_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Enter the page title required for this page or leave blank to use the charity name'),
        '#description' => t('Enter the required page title. if this field is left blank the page title will take the form <br/><strong>Charity Name Personlised Christmas Cards</strong>'),
        '#default_value' => isset($landing_page_term) && isset($landing_page_term->field_page_title[LANGUAGE_NONE][0]['safe_value']) ? $landing_page_term->field_page_title[LANGUAGE_NONE][0]['safe_value'] : '',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
    );

    //Text element for landing url
    $form['url'] = array(
        '#type' => 'textfield',
        '#title' => t('Enter the url for the landing page'),
        '#description' => t('Enter the required url for the landing page. This should be lowercase letters and use hyphens for spaces. <br/>For example <strong>my-landing-page</strong>'),
        '#default_value' => isset($landing_page_term) ? $landing_page_term->name : '',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
    );
    //Page content
    $form['page_content'] = array(
        '#title' => t('Page content'),
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#cols' => 10,
        '#resizeable' => false,
        '#description' => t('Provide the content that you would like displayed in
         the area below the banner image on the landing page'),
        '#default_value' => isset($landing_page_term) ? $landing_page_term->description : '',
    );
    //Banner upload
    if (isset($landing_page_term)) {

        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'slider_images_landing_pages')
            ->propertyCondition('status', 1)
            ->fieldCondition('title_field', 'value', $landing_page_term->name, '=')
            ->addMetaData('account', user_load(1)); // Run the query as user 1.
        $result = $query->execute();

        if (isset($result['node'])) {
            $node = array_values($result['node']);
            $nid = $node[0]->nid;
            $slider = node_load($nid);
            $slider_image = $slider->field_slider_image[LANGUAGE_NONE][0]['fid'];
        }
    }
    $form['banner'] = array(
        '#type' => 'managed_file',
        '#title' => t('Upload Image'),
        '#size' => 48,
        '#default_value' => isset($landing_page_term) ? $slider_image : '',
        '#description' => t('Allowed extensions: <strong>gif png jpg jpeg.</strong>'),
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#theme' => 'fp_slider_breakpoints_theme_admiral_desktop_1x',
        '#upload_location' => 'public://slide-images',
    );

    $form['buttons'] = array(
        '#type' => 'container',
    );

    if ($args == null) {
        $form['buttons']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Add Landing Page'),
            '#name' => 'add',
            '#submit' => array('admiral_landing_pages_add_form_submit'),
            '#validate' => array('admiral_landing_pages_add_form_validate'),
        );
    }

    else {
        //Update Button
        $form['buttons']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Update Landing Page'),
            '#name' => 'update',
            '#submit' => array('landing_page_update'),
            '#validate' => array('admiral_landing_pages_add_form_validate'),
        );
        //Delete button
        /*$form['buttons']['delete'] = array(
            '#type' => 'submit',
            '#value' => t('Delete Landing Page'),
            '#name' => 'delete',
            '#submit' => array('landing_pages_delete'),
        );*/
    }

    return $form;
}


function admiral_landing_pages_add_form_validate($form, &$form_state)
{
    //Check if charity has been entered
    if ($form_state['values']['charity'] == '0') {
        form_set_error('charity', 'Please select a charity');
    }
    //Check if url has been entered
    if ($form_state['values']['url'] == '') {
        form_set_error('url', 'Please enter a url');
    }
    //Check if this a duplicate record
    //Also allow to be altered if updating
    if ($form_state['triggering_element']['name'] = 'update') {
        //Test for valid characters
        if (!preg_match("/^[a-z-]+$/", $form_state['values']['url'], $matches)) {
            form_set_error('url', 'The url can consist of only lowercase letters and hyphens');
        }
    }
    else {
        if ($form_state['values']['url'] != '') {
            $existing_urls = variable_get('admiral_landing_pages', '');
            //Test for duplicate
            if (strpos($existing_urls, $form_state['values']['url']) != false) {
                form_set_error('url', 'You cannot enter the same url twice');
            }
            //Test for valid characters
            if (!preg_match("/^[a-z-]+$/", $form_state['values']['url'], $matches)) {
                form_set_error('url', 'The url can consist of only lowercase letters and hyphens');
            }
        }

    }

    //Check if content has been entered
    if ($form_state['values']['page_content']['value'] == '') {
        form_set_error('page_content', 'Please provide content for the landing page');
    }

    //Check if banner has been uploaded
    if ($form_state['values']['banner'] == '') {
        form_set_error('banner', 'Please provide a banner image for the landing page');
    }

}


function admiral_landing_pages_add_form_submit($form, &$form_state)
{
    //Save the landing page data
    $vocabulary = taxonomy_vocabulary_machine_name_load('landingpages');

    $term = new stdClass();
    $term->name = $form_state['values']['url'];
    $term->vid = $vocabulary->vid;
    $page_title = array(
        'value' => $form_state['values']['page_title'],
        'format' => null,
        'safe_value' => check_plain($form_state['values']['page_title']),
    );
    $term->field_page_title[LANGUAGE_NONE][0] = $page_title;
    $term->description = $form_state['values']['page_content']['value'];
    $term->format = 'full_html';
    $term->admiral_charity_select['und'][0]['tid'] = $form_state['values']['charity'];

    taxonomy_term_save($term);

    //Load the image using the fid in the form_state
    $file = file_load($form_state['values']['banner']);
    $validators = array(
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('png gif jpg jpeg'),
    );

    //Save the file
    file_save_upload($file->uri, $validators, 'public://slide-images/' .  $file->filename, FILE_EXISTS_RENAME);

    //Create ne slider image and save content
    global $user;

    $values = array(
        'type' => 'slider_images_landing_pages',
        'uid' => $user->uid,
        'status' => 1,
        'comment' => 1,
        'promote' => 0,
    );

    $entity = entity_create('node', $values);
    //Wrap the entity
    $entity_wrapper = entity_metadata_wrapper('node', $entity);

    $entity_wrapper->title->set($form_state['values']['url']);

    //Not required for this item
    //$entity_wrapper->body->set(array('value' => $form_state['values']['page_content']['value']));

    //Set weight to 1 as we want this slide to show first
    $entity_wrapper->field_weight->set(1);

    //The field_landing_page is a multiselect so requires an array
    $landing_page = array();
    $landing_page[$term->tid] = $term->name;

    $entity_wrapper->field_landing_page->set($term->tid);

    $entity_wrapper->field_slider_image->file->set($file);

    //Save the data
    $entity_wrapper->save();

    //Write the landing pages variable back
    //Get the url value
    $url = $form_state['values']['url'];

    //Delete the existing list of terms
    variable_delete('admiral_landing_pages');

    //Load all terms from landing page vocab
    $terms = taxonomy_get_tree($vocabulary->vid);

    foreach($terms as $term) {
        $pages[] = $term->name;
    }

    $new_url_list = implode(',', $pages);

    variable_set('admiral_landing_pages', $new_url_list);

    //Rebuild the menus
    menu_rebuild();

    drupal_set_message(t('The page has been added'), 'status');
}

function admiral_landing_pages_delete_form($form, &$form_state) {

    //If its the first load of the form set the storage step state to step1
    if (empty($form_state['storage']['step'])) {
        $form_state['storage']['step'] = 1;
    }

    if ($form_state['storage']['step'] == 1) {
        //Load the list of landing pages from the admiral_landing_pages variable
        $pages = variable_get('admiral_landing_pages', '');

        $pages = explode(',', $pages);


        $header = array();
        $rows = array();

        //Build the header
        $header = array(
            'name' => t('Page Name'),
            'link' => t('Delete'),
        );

        //Split the pages into rows
        foreach ($pages as $key => $page) {
            $rows[] = array(
                'id' => $key,
                'name' => l($page, $page),
                'link' => l('edit/delete', 'admin/config/landing_pages/landingpages/' . $key),
            );
        }

        //Create options keyed by ids
        $options = array();
        foreach ($rows as $row) {
            $options[$row['id']] = array(
                'name' => $row['name'],
                'link' => $row['link'],
            );
        }

        //Create the table
        $form['table'] = array
        (
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $options,
            '#empty' => t('No users found'),
        );

        //Add a delete button
        $form['submit'] = array
        (
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#name' => 'delete',
            '#submit' => array('admiral_landing_pages_delete_form_submit'),
        );
    }

    if ($form_state ['storage']['step'] == 2) {
        /*$form['description'] = array(
            '#type' => 'item',
            '#title' => t('Confirm Step'),
        );*/

        $values = $form_state['values']['table'];

        $items = array();

        foreach ($values as $key => $value) {
            //Collect the terms that have been selected
            if ($value > 0) {
                $items[] = $key;
            }
        }

        $form_state['storage']['values'] = $items;
        //Produce a list of items to be deleted
        $pages = variable_get('admiral_landing_pages', '');
        if ($pages != '') {
            $pages = explode(',', $pages);
            $message_items = array();
            foreach($items as $item){
                $message_items[] = $pages[$item];
            }
            $message_items = implode(', ', $message_items);
        }

        drupal_set_message($message_items, 'status');

        return confirm_form(
            $form,
            t('Are you sure you want to delete this item?'),
            'admin/config/system/deletelandingpages',
            t('This action cannot be undone.'),
            t('Delete'),
            t('Cancel')
        );
    }

    return $form;

}

/**
 * Validate function for delete form
 */
/*function admiral_landing_pages_delete_form_validate($form, &$form_state) {

    //Load the list of landing pages from the admiral_landing_pages variable
    $pages = variable_get('admiral_landing_pages', '');

    $pages = explode(',', $pages);

    $selected = array();

    $items = $form_state['values']['table'];
    foreach($items as $item => $value) {
        if ($value != 0) {
            $selected[] = $item;
        }
    }

    if (empty($selected)) {
        form_set_error('table', t('No items were selected for deletion'));
    }
}*/

/**
 * submit function for deleting items
 */
function admiral_landing_pages_delete_form_submit($form, &$form_state) {
    //Set the form to step 2 if it is on step 1
    if ($form_state['storage']['step'] == 1 && $form_state['triggering_element']['#name'] == 'delete') {
        $form_state['storage']['step'] = 2;
    }
    else {
        //Delete items here
        if ($form_state['values']['confirm'] == 1) {
            //Get items to delete
            $values = $form_state['storage']['values'];
            $items = array();
            //Produce a list of items to be deleted
            $pages = variable_get('admiral_landing_pages', '');
            if ($pages != '') {
                $pages = explode(',', $pages);
                //Get the page names
                foreach($values as $value) {
                    $items[] = $pages[$value];
                }
                foreach($items as $item) {
                    $landing_page = taxonomy_get_term_by_name($item, 'landingpages');
                    if ($landing_page) {
                        $tid = key($landing_page);
                        taxonomy_term_delete($tid);
                    }
                    //Find the node
                    $query = new EntityFieldQuery();
                    $query->entityCondition('entity_type', 'node')
                        ->entityCondition('bundle', 'slider_images_landing_pages')
                        ->propertyCondition('status', 1)
                        ->fieldCondition('title_field', 'value', $item, '=')
                        ->addMetaData('account', user_load(1)); // Run the query as user 1.
                    $result = $query->execute();
                    if ($result) {
                        $nid = key($result['node']);
                        node_delete($nid);
                    }
                    //Rebuild the landing pages variable
                    $vocabulary = taxonomy_vocabulary_machine_name_load('landingpages');
                    //Delete the existing list of terms
                    variable_delete('admiral_landing_pages');
                    //Load all terms from landing page vocab
                    $terms = taxonomy_get_tree($vocabulary->vid);
                    $pages = array();
                    foreach($terms as $term) {
                        $pages[] = $term->name;
                    }
                    $new_url_list = implode(',', $pages);
                    variable_set('admiral_landing_pages', $new_url_list);
                    //Rebuild the menus
                    menu_rebuild();
                }
            }
        }

        //Reset the step to one
        $form_state['storage']['step'] = 1;
    }
    $form_state['rebuild'] = TRUE;
    return;
}

/**
 * submit function for updating items
 */
function landing_page_update($form, &$form_state) {

    $page_id = $form_state['storage']['arg'];
    $pages = variable_get('admiral_landing_pages', '');
    if ($pages != '') {
        $pages = explode(',', $pages);
        $page = $pages[$page_id];
        //Load all the landing page terms
        $page_terms = taxonomy_get_term_by_name($page, 'landingpages');
        $tid = key($page_terms);
        //Load the landing page
        $term = taxonomy_term_load($tid);
        //Find and load the node
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'slider_images_landing_pages')
            ->propertyCondition('status', 1)
            ->fieldCondition('title_field', 'value', $term->name, '=')
            ->addMetaData('account', user_load(1)); // Run the query as user 1.
        $result = $query->execute();

        //$node = node_load(key($result['node']));
        $entity = entity_load_single('node', key($result['node']));
        $entity_wrapper = entity_metadata_wrapper('node', $entity);

        //Set the values on the term
        $term->name = $form_state['values']['url'];
        $page_title = array(
            'value' => $form_state['values']['page_title'],
            'format' => null,
            'safe_value' => check_plain($form_state['values']['page_title']),
        );
        $term->field_page_title[LANGUAGE_NONE][0] = $page_title;
        $term->description = $form_state['values']['page_content']['value'];
        $term->admiral_charity_select[LANGUAGE_NONE][0]['tid'] = $form_state['values']['charity'];

        //Set the values on the node
        $entity_wrapper->title->set($form_state['values']['url']);

        //Set weight to 1 as we want this slide to show first
        $entity_wrapper->field_weight->set(1);

        //The field_landing_page is a multiselect so requires an array ????
        $landing_page = array();
        $landing_page[$term->tid] = $term->name;

        $entity_wrapper->field_landing_page->set($term->tid);

        //Replace image only if its changed
        $current_image = $entity_wrapper->field_slider_image->file->value();

        if ($form_state['values']['banner'] != $current_image->fid) {
            //Load the new image
            $file = file_load($form_state['values']['banner']);

            $validators = array(
                'file_validate_is_image' => array(),
                'file_validate_extensions' => array('png gif jpg jpeg'),
            );

            //Save the file
            file_save_upload($file->uri, $validators, 'public://slide-images/' .  $file->filename, FILE_EXISTS_RENAME);

            $entity_wrapper->field_slider_image->file->set($file);
        }

        //Save the term
        taxonomy_term_save($term);
        
        //Save the node
        $entity_wrapper->save();

        drupal_set_message('The landing page has been updated', 'status');

        //Rebuild the landing pages variable
        $vocabulary = taxonomy_vocabulary_machine_name_load('landingpages');

        //Delete the existing list of terms
        variable_delete('admiral_landing_pages');

        //Load all terms from landing page vocab
        $terms = taxonomy_get_tree($vocabulary->vid);

        $pages = array();

        foreach($terms as $term) {
            $pages[] = $term->name;
        }

        $new_url_list = implode(',', $pages);

        variable_set('admiral_landing_pages', $new_url_list);

        //Rebuild the menus
        menu_rebuild();
    }

}
